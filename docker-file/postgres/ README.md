
<h1>Postgres Docker File</h1>

This folder is customer's postgres docker file which base on original postgres docker file(version 11).

此目錄為客製化的Postgres Docker File , 此目錄案例參考原始的 Postgres 的 Docker File (版本12) 


## 環境建置(build env)


Step1.Clone this Project
```bash
    git clone host:blues.lee/build-env-with-docker
```

Step2.Change Folder to docker-file/postgres
```bash
    cd build-env-with-docker/docker-file/postgres
```

Step3.(Option)Change SQL username , password , dbname  on createdb.sql file
```bash
    vim createdb.sql
    change ...
```

Step4.Build and set image name with docker
```bash
    docker build . -t <Image Name>
```

Step5.When finish docker build and run container
(please ref postgres docker official images page [postgres-docker-file](https://hub.docker.com/_/postgres))
```bash
    docker run -d \
    --name <container name> \
    <image name>
```

